
import subprocess

path = "../ELITR/elitr-testset/indices/en-asr-computational-linguistics"

for line in open(path,"r"):
    line = line.strip()
    if "OSt" in line:
        subprocess.call(["cp","/mnt/c/Users/Christian Huber/Documents/ISL/Forschung/ELITR/"+line,"references"])
    elif "mp3" in line:
        subprocess.call(["cp","/mnt/c/Users/Christian Huber/Documents/ISL/Forschung/ELITR/"+line[:-4]+".mp3","audios"])
    elif "wav" in line:
        subprocess.call(["cp","/mnt/c/Users/Christian Huber/Documents/ISL/Forschung/ELITR/"+line,"audios"])
        
