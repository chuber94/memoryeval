
To run the system do the following steps:

1) put the reference file to references/*.OSt with python get_references.py
2) python extract_terms.py && python terms_to_memory.py
3) create a new folder model* and put the hypothesis file to model*/*.output with
   bash run_test.sh model* terms=0|1|2 speed=1 textfileid=1|2 before_after=0|1|2 [audiopath]
4) python eval.py model* [verbose]

-> mWER, new_words_acc, false_pos