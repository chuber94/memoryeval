
import sys
import glob
import subprocess
from mwerSegmenter.normalizeText import strip_accents
import os.path

if len(sys.argv)<2:
    print("Usage: python eval.py model [verbose]")
    sys.exit()
model = sys.argv[1]

verbose1 = sys.argv[2] in ["verbose","verbose1"] if len(sys.argv)>=3 else False
verbose2 = sys.argv[2] in ["verbose","verbose2"] if len(sys.argv)>=3 else False

for ref_file in glob.glob("references/*.OSt"):
    new_words_file = "terms/"+ref_file[len("references/"):]+".terms"
    new_words = [strip_accents(line.strip()).lower().split("<term>") if line.strip()!="" else [] for line in open(new_words_file,"r")]
    new_words_unique = set([w for ws in new_words for w in ws])

    hypo_file = model+"/"+ref_file[len("references/"):-4]+".output"
    if not os.path.isfile(hypo_file):
        print("Hypo file corresponding to",ref_file,"not found")
        continue

    output = subprocess.run(["./mwerSegmenter/align.sh",ref_file,hypo_file], capture_output=True).stderr.decode()
    subprocess.run(["cp","mwerSegmenter/__segments",model+"/"+ref_file[len("references/"):-4]+".segment_output"])

    if model == "modelEmptyMemory":
        timestamp_file = model+"/"+ref_file[len("references/"):-4]+".output_timestamp"
        lines_h = [(w,[int(x) for x in ts.strip().split()]) for line_h,ts in zip(open("mwerSegmenter/__hypo.txt","r"),open(timestamp_file,"r")) if line_h.strip()!="" for w in line_h.strip().split()]
        lines_seg = [(w,i) for i,line_seg in enumerate(open("mwerSegmenter/__segments","r")) for w in line_seg.strip().split()]

        with open(model+"/"+ref_file[len("references/"):-4]+".segments_timestamp","w") as f:
            i = 0
            for line_seg in open("mwerSegmenter/__segments","r"):
                f.write(str(lines_h[i][1][0])+" ")
                for w in line_seg.strip().split():
                    if w!="":
                        i += 1
                        i = min(i, len(lines_h)-1)
                f.write(str(lines_h[i][1][1])+"\n")

    mWER = float(output.strip().split()[-1])

    hypo = [line.strip() for line in open("mwerSegmenter/__segments","r")]
    ref = [line.strip() for line in open("mwerSegmenter/__ref.txt","r")]

    anz_new_words = sum([len(ws) for ws in new_words])
    anz_words = sum([len(line.split()) for line in ref])
    correct = 0
    false_positives = 0
    for ws,h,r in zip(new_words,hypo,ref):
        for w in ws:
            if w in h:
                correct += 1
            else:
                if verbose1:
                    print("True negative:\n    New word:",w,"\n    Hypo:",h,"\n    Ref: ",r)

        for w2 in new_words_unique:
            if w2 in h and w2 not in r:
                false_positives += 1
                if verbose2:
                    print("False positive:\n    New word:",w2,"\n    Hypo:",h,"\n    Ref: ",r)
                    
    print("mWER: %5.2f"%mWER,
          ", new_words_acc: %5.2f"%(100*correct/anz_new_words)+" %",
          ", false_pos: %3d"%false_positives,
          ", anz_new_words: %3d"%anz_new_words,
          ", anz_words: %5d"%anz_words,
          ref_file[len("references/"):])

    if verbose1 or verbose2:
        print("------------------------------------------------------------------------------------------------------------------------------------------------------------")
