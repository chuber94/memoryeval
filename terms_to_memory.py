
import glob
import pickle
import re
from mwerSegmenter.normalizeText import strip_accents

with open("terms/wordsDuringTraining.pkl","rb") as f:
    words_sys = pickle.load(f)

for ref_file in glob.glob("references/*.OSt"):
    new_words_file = "terms/"+ref_file[len("references/"):]+".terms"

    words_doc = set(nw for line in open(new_words_file,"r") for nw in line.strip().split("<term>") if len(nw)>0)

    with open(new_words_file+"_memory","w") as f:
        for w in words_doc:
            f.write(w+"\n")

for ref_file in glob.glob("references/*.OSt"):
    new_words_file = "terms/"+ref_file[len("references/"):]+".terms_extended"

    words_doc = set(nw for line in open(new_words_file,"r") for nw in line.strip().split("<term>") if len(nw)>0)

    with open(new_words_file+"_memory_extended","w") as f:
        for w in words_doc:
            f.write(w+"\n")