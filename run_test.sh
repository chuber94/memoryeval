#!/bin/bash

# usage: bash run_test.sh model terms=0|1|2 [speed=-1|1|? [textfileid=1|2 [before_after=0|1|2 [audiopath]]]]

model=$1
terms=$2
speed=${3:-1} # e.g. 5 for 5x live speed
textfileid=${4:-1}
before_after=${5:-1}
audio=${6:-audios/*}

mkdir -p $model

for x in $audio
do
    echo "Working on file $x"

    x2=`echo $x | cut -d"/" -f 2- | rev | cut -d"." -f2- | rev`

    if [ -f "$model/$x2.prc_output" ]; then
        echo "File already decoded, continuing"
        continue
    fi

    echo "Uploading memory terms"
    if [ "$terms" = 0 ] || [ "$before_after" != 0 ]
    then
        scp terms/empty.terms_memory i13:/project/OML/chuber/LT/EN-NewWords-BaselineLSTM-MemoryTF/words$textfileid.txt
    elif [ "$terms" = 1 ]
    then
        scp terms/$x2.OSt.terms_memory i13:/project/OML/chuber/LT/EN-NewWords-BaselineLSTM-MemoryTF/words$textfileid.txt
    else
        scp terms/$x2.OSt.terms_extended_memory_extended i13:/project/OML/chuber/LT/EN-NewWords-BaselineLSTM-MemoryTF/words$textfileid.txt
    fi
    echo "Decoding file"
    python -u -W ignore run_test.py $x $speed en-EU-memory$textfileid $model $before_after $terms $textfileid | tee $model/$x2.prc_output
    echo "Postprocess output"
    python prc_output_to_dot_output.py $model/$x2.prc_output
done
