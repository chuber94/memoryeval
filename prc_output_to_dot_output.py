
import sys
import time

def lines_to_unique(log):
    output = []
    i = 0
    for i in range(len(log)-1):
        if log[i+1][0]>=log[i][1]:
            output.append(log[i])
        #elif log[i+1][0]!=log[i][0]:
            #if not log[i][2] in log[i+1][2]:
                #print("WARNING",log[i-1:i+2])
    output.append(log[-1])
    return output

if len(sys.argv)<2:
    print("Usage: python prc_output_to_dot_output.py file.prc_output [timestamps]")
    sys.exit()

inp = sys.argv[1]

lines = [line.strip() for line in open(inp,"r") if line[:3]=="asr"]
lines = [line.replace("<br><br>","") for line in lines]
lines = [[int(s[3][1:-1]),int(s[4][1:-1]),":".join(s[6].split(":")[3:]).strip()] for line in lines if (s:=line.split("|"))]
lines = lines_to_unique(lines)

out = ".".join(inp.split(".")[:-1])+".output"

with open(out,"w") as f:
    for _,_,line in lines:
        f.write(line+"\n")

out += "_timestamp"

with open(out,"w") as f:
    for b,e,_ in lines:
        f.write(str(b)+" "+str(e)+"\n")