#!/bin/bash

ref=$1
hypo=$2

cd mwerSegmenter

python normalizeText.py ../$ref > __ref.txt
python normalizeText.py ../$hypo > __hypo.txt
./mwerSegmenter -mref __ref.txt -hypfile __hypo.txt -usecase 0

cd ..