
import sys
import unicodedata
import string
import re

def strip_accents(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

def normalize_line(line):
    line = line.strip()
    line = strip_accents(line).lower()
    line = re.sub("[^a-z0-9' ]", '', line).replace("'"," '")
    return line

if __name__ == "__main__":
    for line in open(sys.argv[1],"r"):
        line = normalize_line(line)
        print(line)