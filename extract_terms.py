
import os
import glob
import pickle
import re
from mwerSegmenter.normalizeText import strip_accents

with open("terms/wordsDuringTraining.pkl","rb") as f:
    words_sys = pickle.load(f)

for ref_file in glob.glob("references/*.OSt"):
    new_words_file = "terms/"+ref_file[len("references/"):]+".terms"
    
    words_doc = {}
    with open(new_words_file,"w") as f:
        lines = [re.sub("[^a-zA-Z0-9' ]", " ", strip_accents(line.strip())).replace("'"," '").split() for line in open(ref_file,"r")]
        for ws in lines:
            for w in ws:
                if w.lower() in ["ehm","eeem","eem","eehm","'am","uoouh","uch"]:
                    continue
                if not w.lower() in words_sys:
                    if not w in words_doc:
                        words_doc[w] = 1
                    else:
                        words_doc[w] += 1
        #print(sorted(words_doc.items(),key=lambda x:-x[1]))
        print("New words:",len(words_doc),"in",ref_file)
        for ws in lines:
            first = True
            for w in ws:
                if w in words_doc:
                    if first:
                        f.write(w)
                        first = False
                    else:
                        f.write("<term>"+w)
            f.write("\n")

    if os.path.isfile(new_words_file+"_extended"):
        print("FILE already exitst, continueing:",new_words_file+"_extended")
        continue
    with open(new_words_file+"_extended","w") as f:
        for ws in lines:
            first = True
            for w in ws:
                if w in words_doc:
                    if first:
                        f.write(w)
                        first = False
                    else:
                        f.write("<term>"+w)
            f.write("\n")