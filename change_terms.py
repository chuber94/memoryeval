
import sys
import pickle
import time
import subprocess

model = sys.argv[1]
audio = sys.argv[2]
before_after = sys.argv[3]
terms = sys.argv[4]
textfileid = sys.argv[5]

with open("terms/wordsDuringTraining.pkl","rb") as f:
    words_sys = pickle.load(f)

segments_timestamp = "modelEmptyMemory/"+audio[len("audios/"):-4]+".segments_timestamp"
if terms=="1":
    terms_file = "terms/"+audio[len("audios/"):-4]+".OSt.terms"
elif terms=="2":
    terms_file = "terms/"+audio[len("audios/"):-4]+".OSt.terms_extended"
else:
    raise NotImplementedError

ts = [[int(x)/1000 for x in line.strip().split()] for line in open(segments_timestamp, "r")]
if before_after=="1":
    ts = [x[0] for x in ts]
elif before_after=="2":
    ts = [x[1] for x in ts]
    ts = [0.0] + ts
else:
    raise NotImplementedError
ts = [ts[i+1]-ts[i] for i in range(len(ts)-1)]
if before_after=="1":
    ts = [0.0] + ts
#print(ts)

terms = [line.strip() for line in open(terms_file, "r")]
#print(terms)

for i,t in enumerate(ts):
    print("Sleep")
    time.sleep(t)
    print("Sleep over")

    with open(model+"/tmp.txt","w") as f:
        words_doc = set(nw for line in terms[:i+1] for nw in line.split("<term>") if len(nw)>0)
        for w in words_doc:
            f.write(w+"\n")

        subprocess.Popen("scp "+model+"/tmp.txt"+" i13:/project/OML/chuber/LT/EN-NewWords-BaselineLSTM-MemoryTF/words"+textfileid+".txt", shell=True)









