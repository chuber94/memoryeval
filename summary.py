
import glob
import subprocess
import sys

verbose = sys.argv[1]=="verbose"

for x in glob.glob("model*"):
    cmd = ("python eval.py "+x).split()
    lines = subprocess.run(cmd, capture_output=True).stdout.decode().strip()
    if verbose:
        print(lines,"\n")
    lines = [line for line in lines.split("\n") if len(line.split())==16]

    errors = sum([float(y[1])*int(float(y[14])) for line in lines if (y:=line.split())])
    words = sum([int(line.split()[14]) for line in lines])

    corrects = sum([float(y[4])*int(float(y[11])) for line in lines if (y:=line.split())])
    newwords = sum([int(line.split()[11]) for line in lines])

    fp = sum([int(line.split()[8]) for line in lines])

    if words == 0:
        print("No hypo files found, continuing\n")
        continue

    mWER = errors/words
    nwAcc = corrects/newwords
    print("mWER: %5.2f"%mWER,
          ", new_words_acc: %5.2f"%nwAcc+" %",
          ", false_pos: %3d"%fp,
          ", anz_new_words: %3d"%newwords,
          ", anz_words: %5d"%words, x)
    if verbose:
        print()
