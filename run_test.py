
import subprocess
import time
from threading import Thread
from queue import Queue, Empty
import sys, os, signal

def enqueue_output(stream, queue):
    for line in iter(stream.readline, ""):
        queue.put(line.decode())

audiopath = sys.argv[1]
speed = sys.argv[2]
fingerprint = sys.argv[3]
model = sys.argv[4]
before_after = sys.argv[5]
terms = sys.argv[6]
textfileid = sys.argv[7]

command = "python -u -W ignore ../../../LT/pythonrecordingclient/bin/python_recording_client.py -S i13srv53.ira.uka.de -po -fi "+fingerprint+" -fo en -i ffmpeg --once -f "+audiopath+" --ffmpeg-speed "+speed

pro = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
stream = pro.stdout

if before_after!="0":
    command = "python change_terms.py "+model+" "+audiopath+" "+before_after+" "+terms+" "+textfileid

    pro2 = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)

q = Queue()
t = Thread(target=enqueue_output, args=(stream, q))
t.daemon = True
t.start()

while True:
    print("    Sleep 5")
    time.sleep(5)

    b = False
    try:
        print(q.get_nowait(),end="")
    except Empty:
        b = True

    if b:
        print("    Sleep 30")
        time.sleep(30)
        try:
            print(q.get_nowait(),end="")
        except Empty:
            os.killpg(os.getpgid(pro.pid), signal.SIGTERM)
            if before_after!="0":
                os.killpg(os.getpgid(pro2.pid), signal.SIGTERM)
            sys.exit()

    try:
        while True:
            print(q.get_nowait(),end="")
    except Empty:
        pass
